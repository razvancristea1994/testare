package AngajatiApp.repository;

import AngajatiApp.controller.EmployeeController;
import AngajatiApp.model.Employee;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static AngajatiApp.controller.DidacticFunction.CONFERENTIAR;
import static org.junit.jupiter.api.Assertions.*;

class EmployeeMockTest {

    EmployeeController employeeController;
    EmployeeMock em;


    @BeforeEach
    void setUp() {
        em = new EmployeeMock();
        employeeController = new EmployeeController(em);
    }

    @Test
    void addEmployee_TC1_BB() {
        Employee e = new Employee();
        e.setId(1);
        e.setLastName("Popovici");
        e.setFirstName("Laura");
        e.setCnp("1920821245085");
        e.setFunction(CONFERENTIAR);
        e.setSalary(3000.0);

        try {
            int employeeNumb = employeeController.getEmployeesList().size();
            try {
                em.addEmployee(e);
                assertEquals(employeeNumb + 1, em.getEmployeeList().size());
                System.out.println("succes");
            } catch (Exception a) {
                a.printStackTrace();
                assert (false);
            }
        } catch (Exception a) {
            a.printStackTrace();
        }
    }

    @Test
    void addEmployee_TC2_BB() {
        Employee e = new Employee();
        e.setId(1);
        e.setLastName("Popovici");
        e.setFirstName("Laura");
        e.setCnp("1920821245085");
        e.setFunction(CONFERENTIAR);
        e.setSalary(799.0);

        try {
            int employeeNumb = em.getEmployeeList().size();
            try {
                em.addEmployee(e);
                assertFalse(false);
                assertEquals(employeeNumb, em.getEmployeeList().size());
                System.out.println("Nu a fost adaugat");
            } catch (Exception a) {
                a.printStackTrace();
                assert (true);
                System.out.println("Exceptie");
            }
        } catch (Exception a) {
            a.printStackTrace();
        }
    }

    @Test
    void addEmployee_TC3() {
        Employee e = new Employee();
        e.setId(1);
        e.setLastName("Popovici");
        e.setFirstName("Laura");
        e.setCnp("1920821245085");
        e.setFunction(CONFERENTIAR);
        e.setSalary(800.0);

        try {
            int employeeNumb = employeeController.getEmployeesList().size();
            try {
                em.addEmployee(e);
                assertEquals(employeeNumb + 1, em.getEmployeeList().size());
                System.out.println("succes");
            } catch (Exception a) {
                a.printStackTrace();
                assert (false);
            }
        } catch (Exception a) {
            a.printStackTrace();
        }
    }

    @Test
    void addEmployee_TC4() {
        Employee e = new Employee();
        e.setId(1);
        e.setLastName("Popovici");
        e.setFirstName("Laura");
        e.setCnp("1920821245085");
        e.setFunction(CONFERENTIAR);
        e.setSalary(801.0);

        try {
            int employeeNumb = employeeController.getEmployeesList().size();
            try {
                em.addEmployee(e);
                assertEquals(employeeNumb + 1, em.getEmployeeList().size());
                System.out.println("succes");
            } catch (Exception a) {
                a.printStackTrace();
                assert (false);
            }
        } catch (Exception a) {
            a.printStackTrace();
        }
    }

    @Test
    void addEmployee_TC5() {
        Employee e = new Employee();
        e.setId(1);
        e.setLastName("Popovici");
        e.setFirstName("Laura");
        e.setCnp("1920821245085");
        e.setFunction(CONFERENTIAR);
        e.setSalary(3499.0);

        try {
            int employeeNumb = employeeController.getEmployeesList().size();
            try {
                em.addEmployee(e);
                assertEquals(employeeNumb + 1, em.getEmployeeList().size());
                System.out.println("succes");
            } catch (Exception a) {
                a.printStackTrace();
                assert (false);
            }
        } catch (Exception a) {
            a.printStackTrace();
        }
    }

    @Test
    void addEmployee_TC6() {
        Employee e = new Employee();
        e.setId(1);
        e.setLastName("Popovici");
        e.setFirstName("Laura");
        e.setCnp("1920821245085");
        e.setFunction(CONFERENTIAR);
        e.setSalary(3500.0);

        try {
            int employeeNumb = employeeController.getEmployeesList().size();
            try {
                em.addEmployee(e);
                assertEquals(employeeNumb + 1, em.getEmployeeList().size());
                System.out.println("succes");
            } catch (Exception a) {
                a.printStackTrace();
                assert (false);
            }
        } catch (Exception a) {
            a.printStackTrace();
        }
    }

    @Test
    void addEmployee_TC7() {
        Employee e = new Employee();
        e.setId(1);
        e.setLastName("Popovici");
        e.setFirstName("Laura");
        e.setCnp("1920821245085");
        e.setFunction(CONFERENTIAR);
        e.setSalary(3501.0);

        try {
            int employeeNumb = employeeController.getEmployeesList().size();
            try {
                em.addEmployee(e);
                assertEquals(employeeNumb, em.getEmployeeList().size());
                System.out.println("Nu a fost adaugat");
            } catch (Exception a) {
                a.printStackTrace();
                assert (false);
            }
        } catch (Exception a) {
            a.printStackTrace();
        }
    }

    @Test
    void addEmployee_TC8_BB() {

        Employee e = new Employee();
        e.setId(1);
        e.setLastName("&opovici");
        e.setFirstName("Laura");
        e.setCnp("1920821245085");
        e.setFunction(CONFERENTIAR);
        e.setSalary(3000.0);

        try {
            int employeeNumb = employeeController.getEmployeesList().size();
            try {
                em.addEmployee(e);
                assertEquals(employeeNumb, em.getEmployeeList().size());
                assertFalse(em.addEmployee(e));
                System.out.println("nu a fost adaugat");
            } catch (IllegalArgumentException e1) {
                e1.printStackTrace();
            }
        } catch (Exception a) {
            a.printStackTrace();
        }
    }

    @Test
    void addEmployee_TC9_BB() {

        Employee e = new Employee();
        e.setId(1);
        e.setLastName("!opovici");
        e.setFirstName("Laura");
        e.setCnp("1920821245085");
        e.setFunction(CONFERENTIAR);
        e.setSalary(3000.0);

        try {
            int employeeNumb = employeeController.getEmployeesList().size();
            try {
                em.addEmployee(e);
                assertEquals(employeeNumb, em.getEmployeeList().size());
                assertFalse(em.addEmployee(e));
                System.out.println("nu a fost adaugat");
            } catch (IllegalArgumentException e1) {
                e1.printStackTrace();
            }
        } catch (Exception a) {
            a.printStackTrace();
        }
    }

    @Test
    void addEmployee_TC10() {
        Employee e = new Employee();
        e.setId(1);
        e.setLastName("Alexe");
        e.setFirstName("Laura");
        e.setCnp("1920821245085");
        e.setFunction(CONFERENTIAR);
        e.setSalary(3000.0);

        try {
            int employeeNumb = employeeController.getEmployeesList().size();
            try {
                em.addEmployee(e);
                assertEquals(employeeNumb + 1, em.getEmployeeList().size());
                System.out.println("succes");
            } catch (Exception a) {
                a.printStackTrace();
                assert (false);
            }
        } catch (Exception a) {
            a.printStackTrace();
        }
    }

    @Test
    void addEmployee_TC11() {
        Employee e = new Employee();
        e.setId(1);
        e.setLastName("alexe");
        e.setFirstName("Laura");
        e.setCnp("1920821245085");
        e.setFunction(CONFERENTIAR);
        e.setSalary(3000.0);

        try {
            int employeeNumb = employeeController.getEmployeesList().size();
            try {
                em.addEmployee(e);
                assertEquals(employeeNumb, em.getEmployeeList().size());
                System.out.println("nu a fost adaugat");
            } catch (Exception a) {
                a.printStackTrace();
                assert (false);
            }
        } catch (Exception a) {
            a.printStackTrace();
        }
    }
}