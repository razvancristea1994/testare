package AngajatiApp.repository;

import AngajatiApp.controller.DidacticFunction;
import AngajatiApp.model.Employee;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class modifyEmployeeFunctionTest {

    EmployeeMock employeeMock;
    Employee ion;

    @BeforeEach
    void setup() {

        employeeMock = new EmployeeMock();
        ion = new Employee("Marius", "Pacuraru", "1234567890876", DidacticFunction.ASISTENT, 2500d);
        ion.setId(10);
    }


    @Test
    void modifyEmployeeFunction_TC1() {

        employeeMock.addEmployee(ion);
        assertEquals(DidacticFunction.ASISTENT, employeeMock.getEmployeeList().
                get(employeeMock.getEmployeeList().size() -1).getFunction());

        employeeMock.modifyEmployeeFunction(ion, DidacticFunction.LECTURER);
        assertEquals(DidacticFunction.LECTURER, employeeMock.getEmployeeList().
                get(employeeMock.getEmployeeList().size() -1).getFunction());
    }

    @Test
    void modifyEmployeeFunction_TC2() {

        List<Employee> employees = new ArrayList<>();
        EmployeeMock employeeMock1 = new EmployeeMock(employees);
        employeeMock1.addEmployee(ion);
        assertEquals(DidacticFunction.ASISTENT, ion.getFunction());

        employeeMock1.deleteAll();
        employeeMock1.modifyEmployeeFunction(ion, DidacticFunction.TEACHER);

        assertEquals(0, employeeMock1.getEmployeeList().size());
    }

    @Test
    void modifyEmployeeFunction_TC3() {

        Employee defaultEmployee = null;
        List<Employee> list = employeeMock.getEmployeeList();
        employeeMock.modifyEmployeeFunction(defaultEmployee,DidacticFunction.ASISTENT);
        assertEquals(list, employeeMock.getEmployeeList());
    }
}