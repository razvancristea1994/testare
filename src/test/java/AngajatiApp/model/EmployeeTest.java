package AngajatiApp.model;

import AngajatiApp.controller.DidacticFunction;
import AngajatiApp.validator.EmployeeException;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.time.Duration;

import static java.time.Duration.ofMillis;
import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class EmployeeTest {

    private Employee employee1;
    private Employee employee2;
    private Employee employee3;

    @BeforeEach
    void setUp(){

        employee1 = new Employee("Ion", "Popescu", "111", DidacticFunction.CONFERENTIAR, 1100.0);
        employee2 = new Employee("John", "", "112", DidacticFunction.LECTURER, 1200.0);
        employee3 = new Employee("Johnny", null, "113", DidacticFunction.TEACHER, 1300.0);
    }

    @Test
    @Order(4)
    void getLastName() {

        assertEquals("Popescu", employee1.getLastName());
        assertEquals("", employee2.getLastName());
        assertEquals(null, employee3.getLastName());
    }

    @Test
    @Order(3)
    void setLastName() {

        assertEquals("Popescu", employee1.getLastName());
        employee1.setLastName(null);
        assertEquals(null, employee1.getLastName());

        assertEquals("", employee2.getLastName());
        employee2.setLastName("Popovici");
        assertEquals("Popovici", employee2.getLastName());

        assertEquals(null, employee3.getLastName());
        employee3.setLastName("");
        assertEquals("", employee3.getLastName());
    }

    @Test
    @Order(1)
    void constructorEmployee(){

        assertNotEquals(employee1, employee2);

        Employee employeeCopy = employee1;
        assertEquals(employee1, employeeCopy);

        assertNotEquals(employee1.getLastName(), employee2.getLastName());
        assertEquals(employee1.getLastName(), employeeCopy.getLastName());

        Employee employee4 = new Employee();

        assertNotEquals(employeeCopy, employee4);
    }

    @Test
    @Order(2)
    void timeoutExceeded1() {

        assertTimeout(ofMillis(1), () ->{

            //Thread.sleep(1);
            employee1.getLastName();
        });
    }

    @Test
    @Order(5)
    void getEmployeeFromStringTest(){

        employee1.setCnp("abc");

        Exception exception = assertThrows(EmployeeException.class, () -> {
            Employee.getEmployeeFromString(employee1.toString(),3);
        });

        String expectedMessage = "Invalid line at: 3";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @ParameterizedTest
    @Order(5)
    @CsvSource({"abc", "!?"})
    void getEmployeeFromStringTestParametrized(String cnp){

        employee1.setCnp(cnp);

        Exception exception = assertThrows(EmployeeException.class, () -> {
            Employee.getEmployeeFromString(employee1.toString(),3);
        });

        String expectedMessage = "Invalid line at: 3";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }
}